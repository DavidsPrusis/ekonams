// Handle active language

function handleActiveLanguage() {
  $(document).ready(function () {
    $(".header__language").click(function () {
      $(".header__language").removeClass("active");
      $(this).toggleClass("active");
    });
  });
}

handleActiveLanguage();

// show and hide languages dropdown for mobile version
$(document).ready(function () {
  $(".languages__button").click(function () {
    $(".languages__list").toggleClass("active");
  });
});

$(document).ready(function () {
  let readMoreHtml = $(".right__middle__desc").html();
  let lessText = readMoreHtml.substr(0, 198) + "...";

  if (readMoreHtml.length > 198) {
    $(".right__middle__desc")
      .html(lessText)
      .append(
        '<p class="read-more">Lasīt vairāk<span class="readmore__dropdown"></span></p>'
      );
  }

  $("body").on("click", ".read-more", function () {
    $(this)
      .parent(".right__middle__desc")
      .html(readMoreHtml)
      .append(
        '<p class="read-less">Aizvērt <span class="readmore__dropdown"></span></p>'
      );
  });
  $("body").on("click", ".read-less", function () {
    $(this)
      .parent(".right__middle__desc")
      .html(lessText)
      .append(
        '<p class="read-more">Lasīt Vairāk<span class="readmore__dropdown"></span></p>'
      );
  });
});

// Slider

const woodsDatas = [
  {
    id: 1,
    imgSrc: "images/gulbuve.jpg",
    absoluteNumber: "1",
    title: "Māja Viens",
    desc:
      "Guļbūve ir viena no senākajām ēku konstrukcijām, kura, nezaudējot savu īpašo vērtību un nozīmi, saglabājusies līdz pat mūsdienām. Aizvien vairāk pilsētnieku izjūt nepieciešamību dzīvot ekoloģiski tīrā un mājīgā vidē, būt harmonijā ar dabu, ar sevi. Meistarīgi veidota guļbūve apvienojumā ar modernām tehnoloģijām palīdz šos sapņus realizēt dzīvē.",
  },
  {
    id: 2,
    imgSrc: "images/veggtykkelser.jpg",
    absoluteNumber: "2",
    title: "Māja Divi",
    desc:
      "Guļbūve ir viena no senākajām ēku konstrukcijām, kura, nezaudējot savu īpašo vērtību un nozīmi, saglabājusies līdz pat mūsdienām. Aizvien vairāk pilsētnieku izjūt nepieciešamību dzīvot ekoloģiski tīrā un mājīgā vidē, būt harmonijā ar dabu, ar sevi. Meistarīgi veidota guļbūve apvienojumā ar modernām tehnoloģijām palīdz šos sapņus realizēt dzīvē.",
  },
  {
    id: 3,
    imgSrc: "images/sākums-scaled-1-2160x919.jpg",
    absoluteNumber: "3",
    title: "Māja Tris",
    desc:
      "Guļbūve ir viena no senākajām ēku konstrukcijām, kura, nezaudējot savu īpašo vērtību un nozīmi, saglabājusies līdz pat mūsdienām. Aizvien vairāk pilsētnieku izjūt nepieciešamību dzīvot ekoloģiski tīrā un mājīgā vidē, būt harmonijā ar dabu, ar sevi. Meistarīgi veidota guļbūve apvienojumā ar modernām tehnoloģijām palīdz šos sapņus realizēt dzīvē.",
  },
];

function displayData(index) {
  const cardImage = document.querySelector(".card__image");
  const cardAbsoluteNumber = document.querySelector(".title__absolute__number");
  const cardTitleText = document.querySelector(".title__text");
  const cardDesc = document.querySelector(".card__right__desc");

  cardImage.src = woodsDatas[index].imgSrc;
  cardAbsoluteNumber.innerText = woodsDatas[index].absoluteNumber;
  cardTitleText.innerText = woodsDatas[index].title;
  cardDesc.innerHTML = `<p>${woodsDatas[index].desc}</p>`;
}

let index = 0;
$(document).ready(function () {
  // Click to left side
  $(".arrows__left").click(function () {
    index--;
    if (index < 0) {
      index = woodsDatas.length - 1;
      displayData(index);
    }
    displayData(index);
  });
  // Click to right side
  $(".arrows__right").click(function () {
    index++;
    if (index > woodsDatas.length - 1) {
      index = 0;
      displayData(index);
    }
    displayData(index);
  });
  displayData(index);
});

// menu
function header() {
  $(document).ready(function () {
    $(".hamburger__menu").click(function () {
      $(this).toggleClass("active");
      $(".header__middle").toggleClass("active");
    });
  });
}
header();

function handleTechActive() {
  $(document).ready(function () {
    $(".uniqueTechnologies__dropdown__title").click(function () {
      $(this).toggleClass("active");
      $(this)
        .closest(".uniqueTechnologies__dropdown")
        .find(".uniqueTechnologies__dropdown__items")
        .toggleClass("active");
    });
  });
}
handleTechActive();
